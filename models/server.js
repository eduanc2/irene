require('dotenv').config();


const Twit = require('twit');
const express = require('express');
const axios = require('axios');
const app = express();

const t = new Twit({
    consumer_key: 'FMB1f2UXQbTDCw3ZYEiFNyoHc',
    consumer_secret: 'ZujojmzXxSi0kUdv1DrPP9XMdIP6aKReOVAAIBnxy0cGHDOhZG',
    access_token: '1008191494807867392-IxCulCL1iEwSe7OlSAMQfg9KHysDXU',
    access_token_secret: 'BFt4C6ccOeLFTIki72cB0oC8FscO4TXNcVlt5rw71UJOA',
    timeout_ms: 60 * 1000, // optional HTTP request timeout to apply to all requests.
    strictSSL: true, // optional - requires SSL certificates to be valid.
});

const facebook = {
    app_id: '738215333927458',
    access_token: "EAAKfZAyaZCGiIBAPZBrSkHrewHy7mlBrLuJ8XAMeTKeg7igFQJdNOFUUN2KtDQqFuZCEWXfteOFpDEtMES08e2Ba5owhTy8EwJTAR2IZAFbPnurZAgCfTqzdpqYKFYsPYPKKCxPXXVv6WaXLaFaaGSHJPo6MRyQWXbc6XzrhPj3HtT3UF0Fbe5LthFvyhYDn0olgfQJr81jAZDZD"
};

const instagram = {
    id: "",
    access_token: "",
    user_id: ""
}

var hashTagsArray=[];

class Server {




    constructor() {
        this.app = express();
        this.port = process.env.PORT;
        this.apiTwitter = "";
        this.routes();
    }


    routes() {
        this.app.use(express.json());

        this.app.get('/api-twitter/:tag', (req, res) => {
            console.log(req.params);

            t.get('search/tweets', {
                q: `#${req.params.tag}`,
                count: 100
            }, function (err, data, response) {
                res.json(data.statuses)
            })

        });



        this.app.post('/api-twitter/', (req, res) => {
            const {
                tag,
                date,
                limit
            } = req.body;

            var create_at = date ? `since:${date}` : '';
            var tweets = [];
            t.get('search/tweets', {
                q: `#${tag} ${create_at}`,
                count: limit ? limit : 20
            }, (err, data, response) => {
                tweets = data.statuses;
                res.json(tweets);
            })
        });

        this.app.post('/api-twitter/retweet-count/', (req, res) => {
            const {
                tag,
                date,
                limit
            } = req.body;

            var create_at = date ? `since:${date}` : '';
            var reTweets = [];
            var stast = [];

            t.get('search/tweets', {
                q: `#${tag} ${create_at}`,
                count: limit ? limit : 20
            }, (err, data, response) => {
                reTweets = data.statuses;

                for (let i = 0; i < reTweets.length; i++) {
                    for (let j = 0; j < reTweets.length - 1; j++) {
                        if (reTweets[j].retweet_count < reTweets[j + 1].retweet_count) {
                            var temporal = reTweets[j];
                            reTweets[j] = reTweets[j + 1];
                            reTweets[j + 1] = temporal;
                            stast[j] = {
                                created_at: reTweets[j].created_at,
                                id: reTweets[j].id,
                                text: reTweets[j].text,
                                retweet_count: reTweets[j].retweet_count,
                                favorite_count: reTweets[j].favorite_count
                            };
                        }
                    }
                }
                res.json(stast);
            })
        });

        this.app.get('/facebook-graph/', (req, res) => {
            var url = `https://graph.facebook.com/${facebook.app_id}?metadata=1&access_token=${facebook.access_token}`;
            axios.get(url, null).then((result) => {
                res.json(result.data);
            }).catch((err) => {
                res.json(err);

            });
        });

        this.app.get('/instagram-graph/:tag', (req, res) => {
            var url = `https://graph.facebook.com/v14.0/me/accounts?access_token=${facebook.access_token}`;
            var tag=req.params.tag;
       
            axios.get(url, null).then((result) => {
                const data = result.data.data;
                instagram.id = data[0].id;
                instagram.access_token = data[0].access_token;
               
                var url2 = `https://graph.facebook.com/v14.0/${instagram.id}?fields=instagram_business_account&access_token=${facebook.access_token}`;
                axios.get(url2, null).then((result) => {
                    instagram.user_id=result.data.instagram_business_account.id
                    // this.igHashtagRecentMedia(res,tag);
                    this.igHashtagTopMedia(res,tag).then(data=>{
                        res.json(data);
                    });
                }).catch((err) => {

                    res.json(err);

                });
            }).catch((err) => {
                res.json(err);

            });
        });

    }

    igHashtagRecentMedia(res,tag){
        return new Promise((resolve, reject)=>{
            var url2 = `https://graph.facebook.com/v14.0/ig_hashtag_search?user_id=${instagram.user_id}&q=${tag}&access_token=${facebook.access_token}`;
    
            axios.get(url2, null).then((result) => {
                var hashTagId=result.data.data
                for (let i = 0; i < hashTagId.length; i++) {
                    const hash = hashTagId[i];
                    var url3 = `https://graph.facebook.com/${hash.id}/recent_media?user_id=${instagram.user_id}&fields=caption,children,comments_count,like_count,media_type,media_url,permalink,timestamp&access_token=${facebook.access_token}`;
                   axios.get(url3, null).then((result) => {
                    hashTagsArray[i]=result.data.data
                    resolve(hashTagsArray) 
                   }).catch((err) => {
                        reject(err)       ;    
                   });
                    
                }
            }).catch((err) => {
    
                res.json(err);
    
            });
        })
    }

    igHashtagTopMedia(res,tag){

        return new Promise((resolve, reject)=>{
            var url2 = `https://graph.facebook.com/v14.0/ig_hashtag_search?user_id=${instagram.user_id}&q=${tag}&access_token=${facebook.access_token}`;
    
            axios.get(url2, null).then((result) => {
                var hashTagId=result.data.data
                for (let i = 0; i < hashTagId.length; i++) {
                    const hash = hashTagId[i];
                    var url3 = `https://graph.facebook.com/${hash.id}/top_media?user_id=${instagram.user_id}&fields=caption,children,comments_count,like_count,media_type,media_url,permalink,timestamp&access_token=${facebook.access_token}`;
                   axios.get(url3, null).then((result) => {
                    hashTagsArray[i]=result.data.data
                    resolve(hashTagsArray) 
                   }).catch((err) => {
                        reject(err)       ;    
                   });
                    
                }
            }).catch((err) => {
    
                res.json(err);
    
            });
        })
    }

    

    tweetsAll(twitters) {
        res.json("ok")
        console.log(twitters);
    }




    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor de twitter en puerto', this.port);
        });
    }

    getApiTwetter() {
        return this.apiTwitter;
    }
}

module.exports = Server;